package com.bn.business.service.onpremisepe.model.backend;

import org.springframework.stereotype.Repository;

import com.bn.business.service.onpremisepe.model.api.Metrica;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface MetricaRepository extends JpaRepository<Metrica, Integer> {

}
