package com.bn.business.service.onpremisepe.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class MetricaNotFoundException extends RuntimeException {
	
	public MetricaNotFoundException(String message) {
		super(message);
	}

}
